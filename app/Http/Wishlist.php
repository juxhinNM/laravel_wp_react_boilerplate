<?php

namespace App\Http;

use App\Base\Ajax;
use Symfony\Component\HttpFoundation\Request;
use App\Wishlist as AppWishlist;

/**
 * WishList class
 *
 * @author Fitim Vata <fitim@newmedia.al>
 */
class Wishlist extends Ajax
{
    public $protection = ['public', 'private'];

    /**
     * Handle ajax request
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function handle(Request $request)
    {

        if ($request->getMethod() === Request::METHOD_GET) {
            return json([
                'count' => \App\Wishlist::getInstance()->get_product_count(),
            ])->status(200);
        }

        $wishlist_action = $request->get('wishlist_action', 'add');
        $product_id = $request->get('product_id');
        $success = false;
        if ($wishlist_action === 'add') {
            $success = AppWishlist::getInstance()->add_product($product_id);
        } else {
            $success = AppWishlist::getInstance()->remove_product($product_id);
        }

        return json([
            'success' => (bool) $success,
            'message' => $this->getMessage($success, $wishlist_action),
            'count' => AppWishlist::getInstance()->get_product_count(),
        ])->status(200);
    }
    
    /**
     * Get message
     *
     * @param bool $success
     * @param string $action
     * @return string
     */
    public function getMessage($success, $action)
    {
        if ($action === 'add') {
            return $success ?
                __('Product added to wishlist!', 'papername') : __('Product was not added to wishlist!', 'papername');
        }

        return $success ?
            __('Product removed from wishlist!', 'papername') : __('Product was not removed from wishlist!', 'papername');
    }
}
