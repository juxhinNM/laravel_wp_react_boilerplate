<?php

namespace App\Hooks;

use App\Base\Singleton;


class RegisterStrings extends Singleton
{
    protected function __construct()
    {
        add_action( 'widgets_init', [ $this, 'register_strings' ]);
    }

	public function register_strings(){
	    
		$strings = [ 
			
		];

	    if( $strings )
	        foreach( $strings as $str )
	            pll_register_string( $str, $str, 'papername', false );

	}
}
