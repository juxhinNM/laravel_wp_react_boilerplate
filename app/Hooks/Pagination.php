<?php

namespace App\Hooks;

use App\Base\Singleton;

class Pagination extends Singleton
{
    protected function __construct()
    {
        add_action('wp_pagination', [ $this , 'pagination' ], 10, 1);
    }

    public function pagination($query = false)
    {
        if ($query) {
            $GLOBALS['wp_query'] = $query;
        }

        return the_posts_pagination([
            'mid_size' => 2,
            'prev_text' => __('<', 'papername'),
            'next_text' => __('>', 'papername'),
            'screen_reader_text' => '&nbsp;',
        ]);
    }

     
}
