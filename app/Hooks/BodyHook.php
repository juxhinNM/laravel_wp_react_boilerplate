<?php

namespace App\Hooks;

use App\Base\Singleton;


class BodyHook extends Singleton
{
    protected function __construct()
    {
        add_filter('body_class', [$this, 'body_classes']);

        //change excerpt length
        add_filter('excerpt_length', [$this, 'custom_excerpt_length' ]);
 
        //translate product categories
        //add_filter('get_term', [$this, 'translate_term'], 10, 1); 

        //translate post title
        add_filter('the_title', [ $this, 'set_post_title' ], 50, 2 );

        //translate post content
        add_filter('the_content', [ $this, 'set_post_content'], 50 );

        add_filter('pll_the_language_link',[$this, 'my_link'], 10, 2);

        add_filter('pre_get_posts', [ $this, 'filter_by_lang' ], 10 ,3 );
        
        foreach([
            'woocommerce_get_checkout_page_id',
            'woocommerce_get_cart_page_id',
            'woocommerce_get_myaccount_page_id',
            'woocommerce_get_edit_address_page_id',
            'woocommerce_get_view_order_page_id',
            'woocommerce_get_change_password_page_id',
            'woocommerce_get_thanks_page_id',
            'woocommerce_get_terms_page_id',
            'woocommerce_get_pay_page_id',
            'woocommerce_get_shop_page_id'
        ] as $filter_name )
        add_filter( $filter_name, array( $this, 'pll_get_page_id' ) );
    }
    
    /**
     * Adds custom classes to the array of body classes.
     *
     * @param array $classes Classes for the body element.
     * @return array
     */
    public function body_classes($classes)
    {
        return $classes;
    }

    public function custom_excerpt_length( $length ) {
        if ( is_admin() ) {
            return $length;
        }
        return 14;
    }

    // public function translate_term($term){
    //     if(pll_current_language()== 'it' ){
    //         $name=get_field('category_title_sq', $term); 
    //         if (! $name){
    //             $name=get_field('post_title_sq', $term);
    //         }
    //         if ($name) {
    //             $term->name = $name;
    //         }
    //     }
    //     return $term;
    // }

    public function post_type_list(){
		return [ 
			'page', 'product'
		];
	}

	public function set_post_title( $title, $id ){

		if(!$id)
			return $title;
		
		$post_type_list = $this->post_type_list();
		$post_type = get_post_type( $id );
		$lang = pll_current_language();

		if(!in_array($post_type , $post_type_list) ){
			return $title;
		}
		if( $lang == 'en' && get_field( 'post_title_en', $id )){
			$title = get_field( 'post_title_en', $id  );
        }
        elseif($lang == 'es' && get_field('post_title_es', $id )){
            $title = get_field('post_title_es', $id);
        }
        elseif($lang == 'ru' && get_field('post_title_ru', $id)){
            $title = get_field('post_title_ru', $id);
        }
        return $title;
    }
    
    public function set_post_content( $content ){
		if( (is_shop() || is_page() || is_single() ) && in_the_loop() ){
			global $post;
			$lang = pll_current_language();
			if( $lang == 'en' && get_field( 'post_content_en', $post->ID ) )
                $content = get_field( 'post_content_en', $post->ID );
            elseif( $lang == 'es' && get_field( 'post_content_es', $post->ID ) )
                $content = get_field('post_content_es', $post->ID);
            elseif( $lang == 'ru' && get_field('post_content_ru', $post->ID) )
                $content = get_field('post_content_ru', $post->ID);
		}
		
		return $content;
	}
    public function term_link_filter( $url, $term, $taxonomy ) {
        return str_replace('categoria-di-prodotto','product-category', 'categoria-de-producto','kategoriya-produkta', $url);
    }

    public function filter_by_lang( $query ){
		if( !is_admin() && ( is_shop() || is_archive() || is_search() ) && $query->is_main_query() ){
            $lang = pll_current_language();
			if( $lang == 'en' ){
                $query->set( 'lang', 'en' );
                $query->set('tax_query', []);
            }
		}
		return $query;
    } 
    public function my_link($url, $slug) {
        return $url === null ? home_url('?lang='.$slug) : $url;
    }

    public function pll_get_page_id( $id ){
        return pll_get_post( $id );
    }

}

