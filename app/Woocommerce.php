<?php

namespace App;


class Woocommerce {
    public static $container_class = '';
    public static function add_featured_product_class_filter($class = 'featured-product-container')
    {
        static::$container_class = $class;
        add_filter(app_prefix() . '_product_loop_container_class', [static::class, 'add_featured_product_class'], 10);
    }

    public static function remove_featured_product_class_filter()
    {
        static::$container_class = '';
        remove_filter(
            app_prefix() . '_product_loop_container_class',
            [static::class, 'add_featured_product_class'],
            10
        );
    }

    public static function add_featured_product_class()
    {
        $class = static::$container_class;
        return "{$class}";
    }
}