<?php

namespace App;

use App\Base\Singleton;

class Wishlist extends Singleton
{
    const ENDPOINT = 'wish-list';
    const DB_KEY = '_wish_list_data';

    protected $user_id;

    protected function __construct()
    {
        $this->user_id = get_current_user_id();
        add_action('init', [$this, 'add_endpoint']);
        add_filter('query_vars', [$this, 'add_query_vars'], 0);
        add_filter('woocommerce_get_query_vars', [$this, 'add_wc_query_vars'], 0);
        add_filter('woocommerce_account_menu_items', [$this, 'add_in_menu']);
        add_action('woocommerce_account_' . static::ENDPOINT . '_endpoint', [$this, 'render']);
        add_filter('woocommerce_endpoint_' . static::ENDPOINT . '_title', [$this, 'add_account_title'], 10, 2);
    }

    public function add_endpoint()
    {
        add_rewrite_endpoint(static::ENDPOINT, EP_ROOT | EP_PAGES);
    }

    /**
     * Add new query var.
     *
     * @param array $vars
     * @return array
     */
    public function add_query_vars($vars)
    {
        $vars[] = self::ENDPOINT;
        return $vars;
    }

    public function add_wc_query_vars($endpoints)
    {
        $endpoints[self::ENDPOINT] = self::ENDPOINT;
        return $endpoints;
    }

    /**
     * Insert the new endpoint into the My Account menu.
     *
     * @param array $items
     * @return array
     */
    public function add_in_menu($items)
    {
        $logout = null;
        if (isset($items['customer-logout'])) {
            $logout = $items['customer-logout'];
            unset($items['customer-logout']);
        }

        $items[self::ENDPOINT] = __('Wish List', 'papername');
        if ($logout) {
            $items['customer-logout'] = $logout;
        }
        return $items;
    }

    /**
     * @param string $title
     * @param string $endpoint
     */
    public function add_account_title($title, $endpoint)
    {
        if ($endpoint === static::ENDPOINT) {
            return __('Wish List', 'papername');
        }

        return $title;
    }

    public function render()
    {
        $products = $this->get_products();
        echo '<div class="container px-md-2 px-lg-0">';
        echo '<div class="row">';
        Woocommerce::add_featured_product_class_filter(
            'js-wishlist-product wishlist-product'
        );
        foreach ($products as $product) {
            global $post;
            $post = get_post($product);
            setup_postdata($post);
            wc_get_template_part('content', 'product');
        }
        echo '</div>';
        echo '</div>';
        wp_reset_postdata();
        Woocommerce::remove_featured_product_class_filter();
    }

    public function get_url()
    {
        return get_permalink(get_option('woocommerce_myaccount_page_id')) . static::ENDPOINT;
    }


    /**
     * @param string $redirect
     * @return string
     */
    public function get_action_redirect_url($redirect)
    {
        if ($this->user_id) {
            return "#";
        }
        return esc_url(get_permalink(get_option('woocommerce_myaccount_page_id')) . "?redirect={$redirect}");
    }

    /**
     * @param string $product_id
     * @return string
     */
    public function get_active_trigger_class($product_id)
    {
        $is_active = $this->has_product($product_id);
        return $is_active ? 'active' : 'inactive';
    }

    /**
     * @return string
     */
    public function get_handle_trigger_class()
    {
        return $this->user_id ? 'js-wishlist-trigger' : '';
    }

    /**
     * @param string $product_id
     */
    public function has_product($product_id)
    {
        return \in_array($product_id, $this->get_products());
    }

    /**
     * @return int
     */
    public function get_product_count()
    {
        // if(isset(static::$cache['_count'])) {
        // return static::$cache['_count'];
        // }
        $count = count($this->get_products());
        // static::$cache['_count'] = $count;
        return $count;
    }

    /**
     * @param int $product_id
     *  @return null|int|bool
     */
    public function add_product($product_id = null)
    {
        $user_id = $this->user_id;
        if (!$user_id) {
            return;
        }

        if (!$product_id) {
            return;
        }

        $product_ids = $this->get_product_ids();

        $exists = (bool)count(array_filter($product_ids, function ($p) use ($product_id) {
            return $p['product_id'] == $product_id;
        }));

        if ($exists) {
            return true;
        }

        $product_ids[] = [
            'product_id' => $product_id,
            'created_at' => (new \DateTime),
        ];
        return update_user_meta($user_id, static::DB_KEY, $product_ids);
    }

    /**
     * @param int $product_id
     * @return null|int|bool
     */
    public function remove_product($product_id = null)
    {
        $user_id = $this->user_id;
        if (!$user_id) {
            return;
        }

        if (!$product_id) {
            return;
        }

        $product_ids = $this->get_product_ids();

        $product_ids = array_values(array_filter($product_ids, function ($p) use ($product_id) {
            return $p['product_id'] != $product_id;
        }));

        return update_user_meta($user_id, static::DB_KEY, $product_ids);
    }

    /**
     * @return array
     */
    protected function get_product_ids()
    {
        $user_id = $this->user_id;
        if (!$user_id) {
            return [];
        }

        $product_ids = get_user_meta($user_id, static::DB_KEY, true);
        if (!$product_ids) {
            return [];
        }

        return $product_ids;
    }

    /**
     * @return array
     */
    protected function get_products()
    {
        $products = array_map(function ($p) {
            return $p['product_id'];
        }, $this->get_product_ids());

        return $products;
    }
}
