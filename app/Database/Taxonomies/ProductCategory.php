<?php

namespace App\Database\Taxonomies;

use App\Base\Database\Taxonomy;

class ProductCategory
{
    public function create()
    {
        return Taxonomy::create()
            ->set_object_type(['product'])
            ->slug('product_category')
            ->name(__('Product Category', 'papername'))
            ->register();
    }
}