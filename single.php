<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package NMC_Theme
 */

get_header();
while(have_posts()) {
    the_post();
    the_title('<h1>', '</h1>');
    the_content();
}
get_footer();
