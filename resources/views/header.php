<header>
    HEADER
    <?php
    wp_nav_menu(array(
        'theme_location' => 'primary',
        'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
        'container' => 'div',
        'container_id' => 'primary-menu',
        'menu_class' => 'nav',
        'fallback_cb' => 'App\Theme\NavWalker::fallback',
        'walker' => new App\Theme\NavWalker(),
    ));
    ?>
</header>
