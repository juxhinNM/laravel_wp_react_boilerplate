<?php
	use App\Wishlist;
	
	global $product;
	$is_active = Wishlist::getInstance()->has_product($product->get_id());
	$active_class = Wishlist::getInstance()->get_active_trigger_class($product->get_id());
	$url = Wishlist::getInstance()->get_action_redirect_url( get_permalink( $product->get_id() ) );
	$trigger_class = Wishlist::getInstance()->get_handle_trigger_class();
?>

<a href="<?php echo $url; ?>" class="<?php echo $trigger_class ?> btn wishlist-button-icon <?php echo $active_class; ?>" data-active="<?php echo $is_active ? 'true': 'false'; ?>" data-product-id="<?php echo $product->get_id() ?>">
    <span class="icon">
		<?php svg_icon('heart-solid', [], ['solid-icon', 'svg-icon--baseline']); ?>
		<?php svg_icon('heart-regular', [], ['outline-icon', 'svg-icon--baseline']); ?>
    </span>
</a>