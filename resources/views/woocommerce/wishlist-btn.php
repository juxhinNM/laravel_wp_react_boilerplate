<?php
	use App\Wishlist;
	
	global $product;
	$is_active = Wishlist::getInstance()->has_product($product->get_id());
	$active_class = Wishlist::getInstance()->get_active_trigger_class($product->get_id());
	$url = Wishlist::getInstance()->get_action_redirect_url( get_permalink( $product->get_id() ) );
	$trigger_class = Wishlist::getInstance()->get_handle_trigger_class();
?>
<div class="wc_fragment_wish_list_btn single-product-info-btn wishlist mb-3">
	<div class="single-product-info-btn__container">
		<div class="single-product-info-btn__info">
			<p><?php echo __('Baby coming?', 'papername'); ?></p>
		</div>
		<div class="single-product-info-btn__button">
			<a
				href="<?php echo $url ?>"
				class="<?php echo $trigger_class; ?> btn btn--flat wishlist-button <?php echo $active_class; ?>"
				data-active="<?php echo $is_active ? 'true': 'false'; ?>"
				data-product-id="<?php echo $product->get_id() ?>"
			>
				<span class="icon">
                    <?php svg_icon('heart-solid', [], ['solid-icon']); ?>
                    <?php svg_icon('heart-regular', [], ['outline-icon']); ?>
				</span>
				
				<span class="text add-text"><?php echo __('Add to wishlist', 'papername'); ?></span>
				<span class="text remove-text"><?php echo __('Remove from wishlist', 'papername'); ?></span>
			</a>
		</div>
	</div>
</div>