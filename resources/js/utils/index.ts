import page from "./Page";
import notices from "./Notices";
import Sliders from "./Sliders";

export default {
  ...page,
  ...notices,
  Sliders,
};
