// import alertify from 'alertifyjs'
import Noty from "noty";

const defaultOptions = {
  theme: "papername",
  layout: "bottomRight",
  closeWith: ["button", "click"],
  timeout: 5000,
};

function _show(text, type = "info", options) {
  new Noty({
    ...defaultOptions,
    ...options,
    type: type,
    text: text,
  }).show();
}

function show(msg, type = "info", options = {}) {
  if (Array.isArray(msg)) {
    msg.forEach((message) => {
      _show(message, type, options);
    });
  }
  if (typeof msg === "string") {
    _show(msg, type, options);
  }
}

export default {
  show,
  error: (msg) => {
    show(msg, "error");
  },
  success: (msg) => {
    show(msg, "success");
  },
};
