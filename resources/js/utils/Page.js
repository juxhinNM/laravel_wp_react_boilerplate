const page = document.documentElement || document.querySelector("html");

let prevState;

function disableScroll() {
  if (page) {
    prevState = page.style.overflowY;
    page.style.overflowY = "hidden";
  }
}

function enableScroll() {
  if (page) {
    page.style.overflowY = prevState || "";
    prevState = null;
  }
}

export default {
  page,
  disableScroll,
  enableScroll,
};
