import reactApp from "./reactApp";
import WishList from "./WishList";
import DocumentElement from "../utils/Page";

export default {
  ...reactApp,
  WishList,
  DocumentElement,
};
