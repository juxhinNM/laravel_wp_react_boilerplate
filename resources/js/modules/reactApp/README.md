This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run build:style`

Separate command to compile the style.<br>
Run separately or with `npm start` it will build the style un-optimized.<br>
Running with `npm build` it will use `purgecss` to optimize the style.<br>
Watch [PurgeCss documentation](https://purgecss.com/) for more information.

### `npm start`

1- Builds un-optimized style.<br>
2- Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

1- Builds optimized style.<br>
2- Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `storybook`

Command used to create components independently and showcase components interactively in an isolated development environment.<br>
Storybook runs outside of the main app so users can develop UI components in isolation without worrying about app specific dependencies and requirements.<br>
Visit [Storybook documentation](https://storybook.js.org/docs/basics/introduction/) for more information about the library.

### `format-all`

Command used to format the whole project code.<br>
Watch `.prettierrc.json` at root level to modify the configuration.<br>
Visit [Prettier documentation](https://prettier.io/) for more information about the library.

### `npm run clean-npm`

Utility command in linux/mac to clean `node_modules` and `package-lock.json`.<br>

### `npm run jsdoc`

Generates static `html` files project documentation under `out` folder at project root level.<br>
Watch `jsdocConfig.json` at root level to modify the configuration.<br>
Visit [JsDoc documentation](https://jsdoc.app/) for more information about the library.

## Learn More

You can learn more about the boilerplate in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
