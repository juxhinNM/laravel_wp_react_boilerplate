window.React = require("react");
window.ReactDOM = require("react-dom");
import App from "./App";

let Element = window.ReactDOM.render(
    <App/>,
    document.getElementById("papername-gift")
);

export default Element;
