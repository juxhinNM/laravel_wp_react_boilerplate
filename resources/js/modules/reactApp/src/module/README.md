## **Notes**

Global app providers are listed here, at RootContainer.

Each of modules if needed must have their own context in their directory.
This choice is applied to separate the logic from the views but also keep global contexts clear and concise.
