import React from "react";
import { UserProvider } from "../provider";
import auth from './auth'

interface RootContainerProps {}

function RootContainer() {
  return (
    <UserProvider>
      <auth.AuthScreen />
    </UserProvider>
  );
}

export default RootContainer;
