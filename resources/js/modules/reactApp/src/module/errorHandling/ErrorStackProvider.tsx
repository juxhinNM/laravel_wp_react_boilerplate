import React, { createContext } from "react";

interface ErrorStackProviderProps {
  children: JSX.Element;
}

interface ErrorStackContextTypes {}

const ERROR_STACK_CONTEXT_INITIAL_VALUES = {};

export const ErrorStackContext = createContext<ErrorStackContextTypes>({
  ...ERROR_STACK_CONTEXT_INITIAL_VALUES,
});

function ErrorStackProvider({
  children,
}: ErrorStackProviderProps): JSX.Element {
  const providerValue = {};

  return (
    <ErrorStackContext.Provider value={providerValue}>
      {children}
    </ErrorStackContext.Provider>
  );
}

export default ErrorStackProvider;
