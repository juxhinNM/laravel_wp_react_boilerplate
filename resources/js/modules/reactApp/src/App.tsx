import React from "react";
import RootContainer from "./module/RootContainer";

interface AppProps {}

function App() {
  return (
    <div className="papername">
        <RootContainer />
    </div>
  );
}

export default App;
