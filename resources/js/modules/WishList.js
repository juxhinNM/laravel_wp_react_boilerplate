import { wp_ajax } from "../wp";
import Notices from "../utils/Notices";

class WishList {
  constructor() {
    this.toggleSelector = ".js-wishlist-trigger";
    this.productSelector = ".js-wishlist-product";
    this.countSelector = ".js-wishlist-count-badge";

    this.handleClick = this.handleClick.bind(this);
    document.body.addEventListener("click", this.handleClick);
  }

  handleClick(event) {
    const triggerBtn = event.target.closest(this.toggleSelector);
    if (!triggerBtn) {
      return;
    }
    const productEl = event.target.closest(this.productSelector);

    event.stopPropagation();
    event.preventDefault();
    const active = triggerBtn.getAttribute("data-active") === "true";
    const productId = triggerBtn.getAttribute("data-product-id") | 0;

    wp_ajax("wishlist")
      .post({
        product_id: productId,
        wishlist_action: active ? "remove" : "add",
      })
      .then((response) => {
        const { data } = response;
        if (!data.success) {
          Notices.error(data.message);
          return;
        }

        triggerBtn.setAttribute("data-active", active ? "false" : "true");

        if (active) {
          triggerBtn.classList.remove("active");
          triggerBtn.classList.add("inactive");
        } else {
          triggerBtn.classList.add("active");
          triggerBtn.classList.remove("inactive");
        }

        if (productEl) {
          productEl.classList.add("hide");
          productEl.addEventListener("animationend", (event) => {
            productEl.remove();
          });
        }
        this.setCount(data.count);
        Notices.success(data.message);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }

  setCount(count) {
    const elements = [...document.querySelectorAll(this.countSelector)];
    elements.forEach((el) => (el.innerHTML = count));
  }
}

export default WishList;
