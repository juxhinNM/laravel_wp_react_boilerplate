import Ajax from "./Ajax";
import Translate from "./Translate";

/**
 *
 * @param {string} action
 */
function wp_ajax(action: any) {
  return Ajax.init(action);
}

export default {
  Ajax,
  Translate,
  wp_ajax,
};
