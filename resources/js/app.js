/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import "./bootstrap";

import wp from "./wp";
import utils from "./utils";
import modules from "./modules";

//const sliders = window.SLIDERS = new Sliders;
(function() {
  // utils.Sliders.add('.js-main-carousel', {
  //   setGallerySize: false,
  //   wrapAround: true,
  // })
  wp.wp_ajax("dummy_ajax2")
    .get()
    .then((response) => console.log(response))
    .catch((error) => console.log(error));
  //console.log(wp.Translate.get("test"));

  /**
   * @module module
   */
  new modules.WishList();

  console.log(wp.Translate.get("test"));
})();
